# parse-config

#### 介绍
springboot动态配置多数据源
主要涉及

1、 application.yml文件中的数据源配置

	datasource:

	    type: com.alibaba.druid.pool.DruidDataSource

	    driverClassName: com.mysql.cj.jdbc.Driver
	    db1:

	      url: jdbc:mysql://192.168.99.16:4000/newsdb?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&useSSL=false&serverTimezone=GMT%2B8

	      username: websis

	      password: websis

	    db2:

	      url: jdbc:mysql://192.168.99.16:4000/newsdb_abroad?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&useSSL=false&serverTimezone=GMT%2B8

	      username: websis

	      password: websis

	    db3:

	      url: jdbc:mysql://192.168.20.8:4000/newsdb_abroad?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&useSSL=false&serverTimezone=GMT%2B8

	      username: websis

	      password: websis

	      
2、com.terren.pc.common.config包下面类

	DataSourceConfig.java(数据库地址配置，数据库名，mapper文件扫描)

	MyBatisMapperScannerConfig.java(Dao类扫描)
	
3、com.terren.pc.common.datasource包下面类

	DataSourceContextHolder.java(数据库选用的方法)

	DynamicDataSource.java(数据源的创建)

	
4、在service层执行相应sql语句前切换需要的数据源DataSourceContextHolder.setDBType(param.getDbType())，不执行此操作则使用默认数据源。