package com.terren.pc.common.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.terren.pc.common.datasource.DynamicDataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;


@Configuration
public class DataSourceConfig {
	
    @Value("${spring.datasource.db1.url}")
    private String db1DBUrl;
    @Value("${spring.datasource.db1.username}")
    private String db1DBUser;
    @Value("${spring.datasource.db1.password}")
    private String db1DBPassword;
    //@Value("${spring.datasource.db1.driver-class-name}")
    //private String db1DBDreiverName;

    @Value("${spring.datasource.db2.url}")
    private String db2DBUrl;
    @Value("${spring.datasource.db2.username}")
    private String db2DBUser;
    @Value("${spring.datasource.db2.password}")
    private String db2DBPassword;

    @Value("${spring.datasource.db3.url}")
    private String db3DBUrl;
    @Value("${spring.datasource.db3.username}")
    private String db3DBUser;
    @Value("${spring.datasource.db3.password}")
    private String db3DBPassword;
   
    @Bean
    public DynamicDataSource dynamicDataSource() {
        DynamicDataSource dynamicDataSource = DynamicDataSource.getInstance();

        DruidDataSource db1DataSource = new DruidDataSource();
        db1DataSource.setUrl(db1DBUrl);
        db1DataSource.setUsername(db1DBUser);
        db1DataSource.setPassword(db1DBPassword);
        //db1DataSource.setDriverClassName(db1DBDreiverName);

        DruidDataSource db2DataSource = new DruidDataSource();
        db2DataSource.setUrl(db2DBUrl);
        db2DataSource.setUsername(db2DBUser);
        db2DataSource.setPassword(db2DBPassword);
        //db2DataSource.setDriverClassName(db2DBDreiverName);
        
        DruidDataSource db3DataSource = new DruidDataSource();
        db3DataSource.setUrl(db3DBUrl);
        db3DataSource.setUsername(db3DBUser);
        db3DataSource.setPassword(db3DBPassword);
        //db3DataSource.setDriverClassName(db3DBDreiverName);
        
        Map<Object,Object> map = new HashMap<>();
        map.put("db1", db1DataSource);
        map.put("db2", db2DataSource);
        map.put("db3", db3DataSource);
        dynamicDataSource.setTargetDataSources(map);
        dynamicDataSource.setDefaultTargetDataSource(db1DataSource);

        return dynamicDataSource;
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory(@Qualifier("dynamicDataSource") DataSource dynamicDataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dynamicDataSource);
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:mybatis/**/*Mapper.xml"));
        return bean.getObject();

    }

    @Bean(name = "sqlSessionTemplate")
    public SqlSessionTemplate sqlSessionTemplate(@Qualifier("sqlSessionFactory") SqlSessionFactory sqlSessionFactory)throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
