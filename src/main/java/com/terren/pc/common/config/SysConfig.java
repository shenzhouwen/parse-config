package com.terren.pc.common.config;

import java.io.File;

public class SysConfig {
	//配置文件路径 
	public static String PROJECT_PATH;// 项目路径
	public static String CONFIG_FILE_NAME = "config" + File.separator + "sysconfig.properties";
	public static String CONTEXT_PATH = "/collection";
	
	//http请求参数
	public static Integer SOCKET_TIME_OUT = 1000 * 300; // http 读取数据timeout
	public static Integer CONNECTION_TIME_OUT = 1000 * 300;// 建立连接timeout
	public static Integer RETRY_COUNT = 3;// 链接重试次数

	// 用户初始密码
	public static String ORIGINAL_SECURE;
	
	//创建的excel文件临时保存路径
	public static String EXCEL_SAVE_PATH;

}
