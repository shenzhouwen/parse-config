package com.terren.pc.common.entity;

public class BasicEntity {
	
	private String dbType;

	public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	
}
