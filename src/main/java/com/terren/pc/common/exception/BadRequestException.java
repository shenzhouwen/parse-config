package com.terren.pc.common.exception;

public class BadRequestException extends Exception {

	/**
	 * 请求错误异常
	 */
	private static final long serialVersionUID = 2250572748169324713L;

	public BadRequestException(String message) {
		super(message);
	}

	public BadRequestException(String message, Throwable cause) {
		super(message, cause);
	}

	public BadRequestException(Throwable cause) {
		super(cause);
	}
}
