package com.terren.pc.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.terren.pc.common.config.SysConfig;


/**
 * 获取接口数据
 */
public class HttpConnectTool {
	static Logger log = LogManager.getLogger(HttpConnectTool.class);

	/**
	 * get方式
	 * 
	 * @param url
	 * @return
	 */
	public static String getHttp(String url) throws Exception {
		HttpClient httpClient = new HttpClient();
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(SysConfig.SOCKET_TIME_OUT);
		httpClient.getHttpConnectionManager().getParams().setSoTimeout(SysConfig.CONNECTION_TIME_OUT);
		GetMethod getMethod = null;
		String dataStr = "";
		try {
			getMethod = new GetMethod(url);
			getMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(SysConfig.RETRY_COUNT, false));// retry 1
			getMethod.addRequestHeader("content-type", "application/json");
			getMethod.addRequestHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
			int statusCode = httpClient.executeMethod(getMethod);
			if (statusCode == HttpStatus.SC_OK) {
				InputStream resStream = getMethod.getResponseBodyAsStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(resStream, "UTF-8"));
				StringBuffer resBuffer = new StringBuffer();
				String resTemp = "";
				while ((resTemp = br.readLine()) != null) {
					resBuffer.append(resTemp);
				}
				dataStr = resBuffer.toString();

			} else if (statusCode == HttpStatus.SC_BAD_REQUEST) {
				log.error("bad request " + statusCode);
			} else {
				log.error("request failed " + statusCode);
			}
		} catch (HttpException e) {
			e.printStackTrace();

			throw new HttpException();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 释放连接
			getMethod.releaseConnection();
		}
		return dataStr;
	}

	/**
	 * post方式
	 * 
	 * @param url
	 * @return
	 */
	public static String postHttp(String url) throws HttpException, IOException, Exception {
		HttpClient httpClient = new HttpClient();
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(SysConfig.SOCKET_TIME_OUT);
		httpClient.getHttpConnectionManager().getParams().setSoTimeout(SysConfig.CONNECTION_TIME_OUT);
		PostMethod postMethod = null;
		String dataStr = "";
		try {
			postMethod = new PostMethod(url);
			postMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(SysConfig.RETRY_COUNT, false));
			postMethod.addRequestHeader("content-type", "application/json");
			postMethod.addRequestHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
			int statusCode = httpClient.executeMethod(postMethod);
			if (statusCode == HttpStatus.SC_OK) {
				InputStream resStream = postMethod.getResponseBodyAsStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(resStream, "UTF-8"));
				StringBuffer resBuffer = new StringBuffer();
				String resTemp = "";
				while ((resTemp = br.readLine()) != null) {
					resBuffer.append(resTemp);
				}
				dataStr = resBuffer.toString();

			} else if (statusCode == HttpStatus.SC_BAD_REQUEST) {
				log.error("bad request" + statusCode);
			} else {
				log.error("request failed" + statusCode);
			}
		} catch (HttpException e) {
			throw new HttpException();
		} catch (IOException e) {
			throw new IOException();
		} catch (Exception e) {
			throw new Exception();
		} finally {
			// 释放连接
			postMethod.releaseConnection();
		}
		return dataStr;
	}
}
