package com.terren.pc.conweb.controller;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.terren.pc.common.util.R;
import com.terren.pc.conweb.entity.ParseConfig;
import com.terren.pc.conweb.service.ParseConfigService;

@Controller
public class ParseConfigController {
	
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ParseConfigService parseConfigService;

	/**
	 * 页面跳转
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/conweb") // http://localhost:8888/pc/conweb
	String returnConweb(Model model, ParseConfig param) {
		try {
			//model.addAttribute("regulartypes", parseConfigService.getRegularTypes(param));
		} catch (Exception e) {
			log.error("ParseConfigController.returnConweb error", e);
			e.printStackTrace();
		}
		return "conweb/conweb";
	}

	@ResponseBody
	@GetMapping("/conweb/get_fail_list")
	public List<ParseConfig> getFailList(ParseConfig param){
		List<ParseConfig> list = null;
		try {
			list = parseConfigService.getFailList(param);
		} catch (Exception e) {
			log.error("ParseConfigController.getFailList error", e);
			e.printStackTrace();
		}
		return list;
	}

	@ResponseBody
	@GetMapping("/conweb/get_regular_list")
	public List<ParseConfig> getRegularList(ParseConfig param){
		List<ParseConfig> list = null;
		try {
			list = parseConfigService.getRegularList(param);
		} catch (Exception e) {
			log.error("ParseConfigController.getRegularList error", e);
			e.printStackTrace();
		}
		return list;
	}

	@ResponseBody
	@GetMapping("/conweb/get_html_list")
	public Map<String,Object> getHtmlList(ParseConfig param){
		List<ParseConfig> list = null;
		ParseConfig p = new ParseConfig();
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			list = parseConfigService.getHtmlList(param);
			p = parseConfigService.getHtmlTotal(param);
		} catch (Exception e) {
			log.error("ParseConfigController.getHtmlList error", e);
			e.printStackTrace();
		}
		map.put("htmls", list);
		map.put("total", p.getN());
		return map;
	}

	@ResponseBody
	@GetMapping("/conweb/get_statistics_list")
	public List<ParseConfig> getHtmlStatisticsList(ParseConfig param){
		List<ParseConfig> list = null;
		try {
			list = parseConfigService.getHtmlStatisticsList(param);
		} catch (Exception e) {
			log.error("ParseConfigController.getHtmlStatisticsList error", e);
			e.printStackTrace();
		}
		return list;
	}

	@PostMapping("/conweb/update_status")
	@ResponseBody
	R updateHtmlStatus(ParseConfig param) {
		try {
			parseConfigService.updateHtmlStatus(param);
			return R.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return R.error(1, "更新失败");
		}
	}

	/**
	 * 显示抓取到的html
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/sourhtml") // http://localhost:8888/pc/conweb
	String sourhtml(ParseConfig param, Model model, HttpServletResponse response) {
		List<ParseConfig> list = null;
		try {
			list = parseConfigService.getHtmls(param);
			// model.addAttribute("html",list.get(0).getHtml());

			// 设置响应内容类型
			response.setContentType("text/html");
			response.setCharacterEncoding("utf-8");
			// 实际的逻辑是在这里
			PrintWriter out = response.getWriter();
			out.println(list.get(0).getHtml());
		} catch (Exception e) {
			log.error("ParseConfigController.returnConweb error", e);
			e.printStackTrace();
		}

		return null;
	}

	/*
	@ResponseBody
	@GetMapping("/conweb/get_htmls")
	public List<ParseConfig> getHtmls(ParseConfig param){
		List<ParseConfig> list = null;
		try {
			list = parseConfigService.getHtmls(param);
		} catch (Exception e) {
			log.error("ParseConfigController.getHtmls error", e);
			e.printStackTrace();
		}
		return list;
	}
	*/
	/*
	@ResponseBody
	@GetMapping("/conweb/get_regular_types")
	public List<Regulartype> getRegularTypes(){
		List<Regulartype> list = null;
		try {
			list = parseConfigService.getRegularTypes();
		} catch (Exception e) {
			log.error("ParseConfigController.getRegularTypes error", e);
			e.printStackTrace();
		}
		return list;
	}
	*/
	@PostMapping("/conweb/save_regular")
	@ResponseBody
	R saveRegular(ParseConfig param, String entryIds) {
		try {
			if(StringUtils.isNotEmpty(entryIds)) {
				parseConfigService.saveRegulars(param,entryIds);
			}else {
				parseConfigService.saveRegular(param);
			}
			return R.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return R.error(1, "保存失败");
		}
	}
	
	@PostMapping("/conweb/update_regular")
	@ResponseBody
	R updateRegular(ParseConfig param) {
		try {
			parseConfigService.updateRegular(param);
			return R.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return R.error(1, "更新失败");
		}
	}
	
	@PostMapping("/conweb/remove_regular")
	@ResponseBody
	R removeRegular(ParseConfig param) {
		try {
			parseConfigService.removeRegular(param);
			return R.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return R.error(1, "删除失败");
		}
	}
	
	@PostMapping("/conweb/remove_fail_htmls")
	@ResponseBody
	R removeFailHtmls(@RequestParam("urlids[]") String[] urlids, @RequestParam("dbType") String dbType) {
		try {
			parseConfigService.removeFailHtmls(urlids, dbType);
			return R.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return R.error(1, "删除失败");
		}
	}
	
	@ResponseBody
	@GetMapping("/conweb/get_entry_list")
	public List<ParseConfig> getEntryList(ParseConfig param) throws Exception {
		List<ParseConfig> list = null;
		try {
			list = parseConfigService.getEntryList(param);
		} catch (Exception e) {
			log.error("ParseConfigController.getEntryList error", e);
			e.printStackTrace();
		}
		return list;
	}
	
	@PostMapping("/conweb/remove_batch")
	@ResponseBody
	R removeHtmlBatch(ParseConfig param) {
		try {
			parseConfigService.removeHtmlBatch(param);
			return R.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return R.error(1, "删除失败");
		}
	}
}
