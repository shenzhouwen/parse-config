package com.terren.pc.conweb.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.terren.pc.conweb.entity.ParseConfig;
import com.terren.pc.conweb.entity.Regulartype;
@Mapper
public interface ParseConfigDao {
	
	List<ParseConfig> getFailList(ParseConfig param) throws Exception;
	
	List<ParseConfig> getRegularList(ParseConfig param) throws Exception;
	
	List<ParseConfig> getHtmlList(ParseConfig param) throws Exception;
	
	void updateHtmlStatus(ParseConfig param) throws Exception;
	
	List<ParseConfig> getHtmlStatisticsList(ParseConfig param) throws Exception;
	
	List<ParseConfig> getHtmls(ParseConfig param) throws Exception;
	
	List<Regulartype> getRegularTypes() throws Exception;
	
	void saveRegular(ParseConfig param) throws Exception;
	
	void updateRegular(ParseConfig param) throws Exception;
	
	void removeRegular(ParseConfig param) throws Exception;
	
	List<ParseConfig> getEntryList(ParseConfig param) throws Exception;
	
	void removeHtmls(List<ParseConfig> items) throws Exception;
	
	void removeUrlids(List<ParseConfig> items) throws Exception;
	
	ParseConfig getHtmlTotal(ParseConfig param) throws Exception;
	
	void removeHtmlBatch(ParseConfig param) throws Exception;
	
}
