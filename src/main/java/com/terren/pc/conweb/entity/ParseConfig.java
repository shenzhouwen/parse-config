package com.terren.pc.conweb.entity;

import com.terren.pc.common.entity.BasicEntity;

public class ParseConfig extends BasicEntity{
	
	private Integer entryId;
	
	private String entryName;
	
	private Integer errorCode;
	
	private String error;
	
	private Integer n;
	
	private String select;
	
	private String attr = "-";
	
	private Integer regulartype;
	
	private String desc;
	
	private String fieldtype;
	
	private String dateformat = "-";
	
	private String urlid;
	
	private String url;
	
	private Integer pagesize;
	
	private String html;
	
	private Integer status;
	
	private String localtm;
	
	private Integer regularId;
	
	private String regex = "-";
	
	private Integer selectorType;
	
	private String entryUrl;
	
	private Integer sourceid;
	
	private String sourcename;
	
	private String urlRegex;

	public Integer getEntryId() {
		return entryId;
	}

	public void setEntryId(Integer entryId) {
		this.entryId = entryId;
	}

	public String getEntryName() {
		return entryName;
	}

	public void setEntryName(String entryName) {
		this.entryName = entryName;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Integer getN() {
		return n;
	}

	public void setN(Integer n) {
		this.n = n;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	public String getAttr() {
		return attr;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}

	public Integer getRegulartype() {
		return regulartype;
	}

	public void setRegulartype(Integer regulartype) {
		this.regulartype = regulartype;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getFieldtype() {
		return fieldtype;
	}

	public void setFieldtype(String fieldtype) {
		this.fieldtype = fieldtype;
	}

	public String getDateformat() {
		return dateformat;
	}

	public void setDateformat(String dateformat) {
		this.dateformat = dateformat;
	}

	public String getUrlid() {
		return urlid;
	}

	public void setUrlid(String urlid) {
		this.urlid = urlid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getPagesize() {
		return pagesize;
	}

	public void setPagesize(Integer pagesize) {
		this.pagesize = pagesize;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getLocaltm() {
		return localtm;
	}

	public void setLocaltm(String localtm) {
		this.localtm = localtm;
	}

	public Integer getRegularId() {
		return regularId;
	}

	public void setRegularId(Integer regularId) {
		this.regularId = regularId;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public Integer getSelectorType() {
		return selectorType;
	}

	public void setSelectorType(Integer selectorType) {
		this.selectorType = selectorType;
	}

	public String getEntryUrl() {
		return entryUrl;
	}

	public void setEntryUrl(String entryUrl) {
		this.entryUrl = entryUrl;
	}

	public Integer getSourceid() {
		return sourceid;
	}

	public void setSourceid(Integer sourceid) {
		this.sourceid = sourceid;
	}

	public String getSourcename() {
		return sourcename;
	}

	public void setSourcename(String sourcename) {
		this.sourcename = sourcename;
	}

	public String getUrlRegex() {
		return urlRegex;
	}

	public void setUrlRegex(String urlRegex) {
		this.urlRegex = urlRegex;
	}
	
	
}
