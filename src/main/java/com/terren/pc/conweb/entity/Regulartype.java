package com.terren.pc.conweb.entity;

import com.terren.pc.common.entity.BasicEntity;

public class Regulartype extends BasicEntity{
	
	private Integer id;
	
	private String name;
	
	private String desc;
	
	private Integer categoryid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getCategoryid() {
		return categoryid;
	}

	public void setCategoryid(Integer categoryid) {
		this.categoryid = categoryid;
	}

}
