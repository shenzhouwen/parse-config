package com.terren.pc.conweb.service;

import java.util.List;

import com.terren.pc.conweb.entity.ParseConfig;
import com.terren.pc.conweb.entity.Regulartype;

public interface ParseConfigService {
	
	List<ParseConfig> getFailList(ParseConfig param) throws Exception;
	
	List<ParseConfig> getRegularList(ParseConfig param) throws Exception;
	
	List<ParseConfig> getHtmlList(ParseConfig param) throws Exception;
	
	void updateHtmlStatus(ParseConfig param) throws Exception;
	
	List<ParseConfig> getHtmlStatisticsList(ParseConfig param) throws Exception;
	
	List<ParseConfig> getHtmls(ParseConfig param) throws Exception;
	
	List<Regulartype> getRegularTypes(ParseConfig param) throws Exception;
	
	void saveRegular(ParseConfig param) throws Exception;
	
	void updateRegular(ParseConfig param) throws Exception;
	
	void removeRegular(ParseConfig param) throws Exception;
	
	List<ParseConfig> getEntryList(ParseConfig param) throws Exception;
	
	void removeFailHtmls(String[] urlids, String dbType) throws Exception;

	void saveRegulars(ParseConfig param, String entryIds) throws Exception;
	
	ParseConfig getHtmlTotal(ParseConfig param) throws Exception;
	
	void removeHtmlBatch(ParseConfig param) throws Exception;
}
