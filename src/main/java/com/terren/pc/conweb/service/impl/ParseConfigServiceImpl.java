package com.terren.pc.conweb.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.terren.pc.common.datasource.DataSourceContextHolder;
import com.terren.pc.conweb.dao.ParseConfigDao;
import com.terren.pc.conweb.entity.ParseConfig;
import com.terren.pc.conweb.entity.Regulartype;
import com.terren.pc.conweb.service.ParseConfigService;

@Service
public class ParseConfigServiceImpl implements ParseConfigService {
	
	@Autowired
	private ParseConfigDao parseConfigDao;

	@Override
	public List<ParseConfig> getFailList(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		return parseConfigDao.getFailList(param);
	}

	@Override
	public List<ParseConfig> getRegularList(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		return parseConfigDao.getRegularList(param);
	}

	@Override
	public List<ParseConfig> getHtmlList(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		return parseConfigDao.getHtmlList(param);
	}

	@Override
	public void updateHtmlStatus(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		parseConfigDao.updateHtmlStatus(param);
		
	}

	@Override
	public List<ParseConfig> getHtmlStatisticsList(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		return parseConfigDao.getHtmlStatisticsList(param);
	}

	@Override
	public List<ParseConfig> getHtmls(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		return parseConfigDao.getHtmls(param);
	}

	@Override
	public List<Regulartype> getRegularTypes(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		return parseConfigDao.getRegularTypes();
	}

	@Override
	public void saveRegular(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		parseConfigDao.saveRegular(param);
	}

	@Override
	public void updateRegular(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		parseConfigDao.updateRegular(param);
	}

	@Override
	public void removeRegular(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		parseConfigDao.removeRegular(param);
	}

	@Override
	public List<ParseConfig> getEntryList(ParseConfig param) throws Exception {
		DataSourceContextHolder.setDBType(param.getDbType());
		return parseConfigDao.getEntryList(param);
	}

	@Override
	public void removeFailHtmls(String[] urlids, String dbType) throws Exception {
		DataSourceContextHolder.setDBType(dbType);
		List<ParseConfig> list = new ArrayList<ParseConfig>();
		for(String urlid : urlids) {
			ParseConfig p = new ParseConfig();
			p.setUrlid(urlid);
			list.add(p);
		}
		parseConfigDao.removeHtmls(list);
		parseConfigDao.removeUrlids(list);
	}

	@Override
	public void saveRegulars(ParseConfig param,String entryIds) throws Exception {
		String[] entryArr = entryIds.split(",");
		for(String entryId : entryArr) {
			param.setEntryId(Integer.parseInt(entryId));
			saveRegular(param);
		}
	}

	@Override
	public ParseConfig getHtmlTotal(ParseConfig param) throws Exception {
		
		return parseConfigDao.getHtmlTotal(param);
	}

	@Override
	public void removeHtmlBatch(ParseConfig param) throws Exception {
		
		parseConfigDao.removeHtmlBatch(param);
		
	}

}
