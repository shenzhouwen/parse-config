// (function($) {
//     // 首先备份下jquery的ajax方法
//     var _ajax = $.ajax;
//
//     // 重写jquery的ajax方法
//     $.ajax = function(options) {
//         // 备份opt中error和success方法
//         var callback = {
//             "beforeSend" : function(XHR) {
//             },
//             "complete" : function(XHR, TS) {
//             },
//             "error" : function(XMLHttpRequest, textStatus, errorThrown) {
//             },
//             "success" : function(data, textStatus) {
//             }
//         }
//
//         // 判断参数中是否有beforeSend回调函数
//         if (options.beforeSend) {
//             callback.beforeSend = options.beforeSend;
//         }
//
//         // 判断参数中是否有complete回调函数
//         if (options.complete) {
//             callback.complete = options.complete;
//         }
//
//         // 判断参数中是否有error回调函数
//         if (options.error) {
//             callback.error = options.error;
//         }
//
//         // 判断参数中是否有success回调函数
//         if (options.success) {
//             callback.success = options.success;
//         }
//
//         // 扩展增强处理
//         var _opt = $.extend(options, {
//             error : function(XMLHttpRequest, textStatus, errorThrown) {
//                 // 错误方法增强处理
//                 callback.error(XMLHttpRequest, textStatus, errorThrown);
//             },
//             success : function(data,textStatus) {
//                 // 成功回调方法增强处理
//                 if(403==data.code){
//                     parent.location.href = '/login';
//                 }
//                 callback.success(data);
//             },
//             beforeSend : function(XHR) {
//                 // 提交前回调方法
//                 var index = layer.load(1, {
//                     shade: [0.1,'#fff'] //0.1透明度的白色背景
//                 });
//                 callback.beforeSend(XHR);
//             },
//             complete : function(XHR, TS) {
//                 // 请求完成后回调函数 (请求成功或失败之后均调用)。
//                 layer.closeAll('loading');
//                 callback.complete(XHR, TS);
//             }
//         });
//
//         // 返回重写的ajax
//         return _ajax(_opt);
//     };
// })(jQuery);
//
//
//
//

//全站ajax加载提示
(function($) {
	$(document).ajaxStart(function() {
		var index = layer.load(1, {
			shade : [ 0.1, '#fff' ]
		//0.1透明度的白色背景
		});
	});
	$(document).ajaxStop(function() {
		layer.closeAll('loading');
	});
	//登录过期，shiro返回登录页面
	$.ajaxSetup({
		complete : function(xhr, status, dataType) {
			if ('text/html;charset=UTF-8' == xhr
					.getResponseHeader('Content-Type')) {
				top.location.href = ctx + 'login';
			}
		}
	});
})(jQuery);

$(".langli",top.document).click(function(){
	changeLang()
});

function changeLang(){
	var langName = $(".langname",top.document).html();
	//console.info(langName.replace(/\s+/g, ""));
	if (langName.replace(/\s+/g, "") == "English") {
		$("[i18n]").i18n({
			defaultLang : "en",
		});
	}
	if (langName.replace(/\s+/g, "") == "Português") {
		$("[i18n]").i18n({
			defaultLang : "pt",
		});
	}
	if (langName.replace(/\s+/g, "") == "中文") {
		$("[i18n]").i18n({
			defaultLang : "cn",
		});
	}
	if (langName.replace(/\s+/g, "") == "Français") {
		$("[i18n]").i18n({
			defaultLang : "fr",
		});
	}
	if (langName.replace(/\s+/g, "") == "Espanol") {
		$("[i18n]").i18n({
			defaultLang : "es",
		});
	}
}
