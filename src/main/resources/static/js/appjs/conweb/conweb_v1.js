var config = {
	get_fail_list : ctx + "conweb/get_fail_list",
	get_regular_list : ctx + "conweb/get_regular_list",//entryId=502
	get_html_list : ctx + "conweb/get_html_list",//entryId=502&errorCode=5
	get_statistics_list : ctx + "conweb/get_statistics_list",
	save_regular : ctx + "conweb/save_regular",
	update_regular : ctx + "conweb/update_regular",
	remove_regular : ctx + "conweb/remove_regular",
	update_status : ctx + "conweb/update_status",
	remove_fail_htmls: ctx + "conweb/remove_fail_htmls",
	get_entry_list: ctx + "conweb/get_entry_list"
};
var regularTable = null;
var htmlTable = null;
var gloablEntryId = null;
var gloablErrorCode = null;

failList();

function failList(){
	$.ajax({
		cache : false,
		type : "GET",
		url : config.get_fail_list,
		data : {},
		async : true,
		error : function(request) {
			layer.alert("Connection error");
		},
		success : function(data){
			failTab(data);
			regularList(data[0].entryId, data[0].entryName, data[0].error, data[0].entryUrl, data[0].sourceid);
			htmlList(data[0].entryId, data[0].errorCode, data[0].entryName, data[0].error);
		}
	});
	
}

function failTab(tableData) {
	$('#failTab').bootstrapTable({
        data:tableData,
        height:890,
        iconSize: 'outline',
        striped: true, // 设置为true会有隔行变色效果
        cache: false,  
        pagination: false, // 设置为true会在底部显示分页条
        // queryParamsType : "limit",
        //设置为limit则会发送符合RESTFull格式的参数
        singleSelect: false, // 设置为true将禁止多选
        // contentType : "application/x-www-form-urlencoded",
        //发送到服务器的数据编码类型
        pageSize: 1000, // 如果设置了分页，每页数据条数
        pageNumber: 1, // 如果设置了分布，首页页码
        // search : true, // 是否显示搜索框
        // showColumns : true, // 是否显示内容下拉框（选择显示的列）
        sidePagination: "client", // 设置在哪里进行分页，可选值为"client" 或者
        onAll: function (name, args) {
        	//全部加载完成后执行
            return false;
        },
        columns: [
            {
                title: '网站名称',
                align: 'center',
                formatter : function(value, row, index) {
                	var temp;
                	if(row.entryName=='' || row.entryName==null){
                		return '';
                	}
                	var title = row.entryName.replace(/"/g,"");
                	if(title.length>6){
                		temp = title.substring(0, 4) + "..."
                	}else{
                		temp = title;
                	}
					return '<a href="#" entryId='+ row.entryId +' sourceid='+ row.sourceid +' entryUrl='+ row.entryUrl +' errorCode='+ row.errorCode +' error='+ row.error +' title="' +title+'" onclick="transReg(this)">' +temp+'</a> ';
				}
            },
            {
				title : '错误类型',
				align : 'center',
				sortable:true,
				field: 'error'
			},
            {
                title: '错误数',
                align: 'center',
                sortable:true,
                field: 'n'
            }]
    });
}

function transReg(t){
	var entryId = $(t).attr("entryId");
	var title = $(t).attr("title");
	var errorCode = $(t).attr("errorCode");
	var error = $(t).attr("error");
	var entryUrl = $(t).attr("entryUrl");
	var sourceid = $(t).attr("sourceid");
	regularList(entryId, title, error, entryUrl,sourceid);
	htmlList(entryId, errorCode, title, error);
}

function regularList(entryId, title, error, entryUrl, sourceid){
	$("#regularTitle").html("解析规则:<a href='"+entryUrl+"' target='_blank'>" + title +"</a>_"+entryId+"-<span style='color:red'>(" +error+ ")</span>");
	entryList(sourceid);
	regularData(entryId);
	
}

function regularData(entryId){
	$.ajax({
		cache : false,
		type : "GET",
		url : config.get_regular_list,
		data : { 
			entryId:entryId
		},
		error : function(request) {
			layer.alert("Connection error");
		},
		success : function(data){
			regularTab(data);
		}
	});
}

function entryList(sourceid){
	$.ajax({
		cache : false,
		type : "GET",
		url : config.get_entry_list,
		data : {sourceid: sourceid},
		async : true,
		error : function(request) {
			layer.alert("Connection error");
		},
		success : function(data){
			var entryHtm ="";
			var entryList = $("#entryList");
			if(data){
				for (var i = 0; i < data.length; i++) {
					var item = data[i];
					entryHtm +="<option value='" + item.entryId + "'>" + item.entryName + "</option>";
				}
			}
			entryList.html(entryHtm);
		}
	});
}

function regularTab(tableData) {
	if(regularTable != null){
		regularTable = $("#regularTab").bootstrapTable('destroy');
	}
	regularTable = $('#regularTab').bootstrapTable({
        data:tableData,
        height:235,
        iconSize: 'outline',
        striped: true, // 设置为true会有隔行变色效果
        cache: false,  
        pagination: false, // 设置为true会在底部显示分页条
        // queryParamsType : "limit",
        //设置为limit则会发送符合RESTFull格式的参数
        singleSelect: false, // 设置为true将禁止多选
        // contentType : "application/x-www-form-urlencoded",
        //发送到服务器的数据编码类型
        pageSize: 200, // 如果设置了分页，每页数据条数
        pageNumber: 1, // 如果设置了分布，首页页码
        // search : true, // 是否显示搜索框
        // showColumns : true, // 是否显示内容下拉框（选择显示的列）
        sidePagination: "client", // 设置在哪里进行分页，可选值为"client" 或者"server"
        columns: [
            {
                title: '说明',
                align: 'center',
                formatter : function(value, row, index) {
                	var temp;
                	if(row.desc=='' || row.desc==null){
                		return '';
                	}
                	var title = row.desc.replace(/"/g,'');
                	if(title.length>12){
                		temp = title.substring(0, 10) + "..."
                	}else{
                		temp = title;
                	}
					return '<a href="#" regularId='+ row.regularId +' title="' +title+'" onclick="dealRegular(this)">' +temp+'</a> ';
				}
            },
            {
				title : '选择器',
				align : 'center',
				formatter : function(value, row, index) {
                	var temp;
                	if(row.select=='' || row.select==null){
                		return '';
                	}
                	var title = row.select;
                	if(title.length>70){
                		temp = title.substring(0, 67) + "..."
                	}else{
                		temp = title;
                	}
					return '<span title="' +title+'">' +temp+'</span> ';
				}
			},
            {
				title : '属性',
				align : 'center',
				field: 'attr'
			},
			{
				title : '正则',
				align : 'center',
				field: 'regex'
			},
			{
				title : '所属类型',
				sortable:true,
				align : 'center',
				field: 'regulartype'
			},
			{
				title : '数据类型',
				sortable:true,
				align : 'center',
				field: 'fieldtype'
			},
            {
                title: '日期格式',
                align: 'center',
                field: 'dateformat'
            },{
				title : '',
				align : 'center',
				formatter : function(value, row, index) {
					return '<a class="btn btn-warning btn-xs" href="#" title="删除"  mce_href="#" onclick="removeRegular('
						+ row.regularId+','+row.entryId+ ')"><i class="fa fa-remove"></i></a> ';
				}
			}]
    });
}

function htmlList(entryId, errorCode, title, error){
	$("#htmlTitle").html("页面内容列表:" + title+"-<span style='color:red'>(" +error+ ")</span>");
	gloablEntryId = entryId;
	gloablErrorCode = errorCode;
	$.ajax({
		cache : false,
		type : "GET",
		url : config.get_html_list,
		data : { 
			entryId:entryId,
			errorCode:errorCode
		},
		error : function(request) {
			layer.alert("Connection error");
		},
		success : function(data){
			htmlTab(data);
		}
	});
}

function htmlTab(tableData) {
	if(htmlTable != null){
		htmlTable = $("#htmlTab").bootstrapTable('destroy');
	}
	htmlTable = $('#htmlTab').bootstrapTable({
        data:tableData,
        iconSize: 'outline',
        striped: true, // 设置为true会有隔行变色效果
        cache: false,  
        pagination: true, // 设置为true会在底部显示分页条
        // queryParamsType : "limit",
        //设置为limit则会发送符合RESTFull格式的参数
        singleSelect: false, // 设置为true将禁止多选
        // contentType : "application/x-www-form-urlencoded",
        //发送到服务器的数据编码类型
        pageSize: 6, // 如果设置了分页，每页数据条数
        pageNumber: 1, // 如果设置了分布，首页页码
        // search : true, // 是否显示搜索框
        // showColumns : true, // 是否显示内容下拉框（选择显示的列）
        sidePagination: "client", // 设置在哪里进行分页，可选值为"client" 或者"server"
        columns: [
            {
                title: '链接',
                align: 'center',
                formatter : function(value, row, index) {
                	var temp;
                	if(row.url.length>70){
                		temp = row.url.substring(0, 67) + "..."
                	}else{
                		temp = row.url;
                	}
					return '<a href="'+row.url+'" target="_blank">' +temp+'</a> ';
				}
            },
            {
				title : '页面大小',
				sortable:true,
				align : 'center',
				field: 'pagesize'
			},
			{
				title : '时间',
				align : 'center',
				sortable:true,
				field: 'localtm'
			},
			{
				title : '抓取页面',
				align : 'center',
				formatter : function(value, row, index) {
                	var temp;
                	if(row.url.length>70){
                		temp = row.url.substring(0, 67) + "..."
                	}else{
                		temp = row.url;
                	}
					return '<a href="'+ctx+'sourhtml?urlid='+row.urlid+'" target="_blank">查看</a> ';
				}
			}]
    });
}

function resetEntry(){
	$.ajax({
		cache : false,
		type : "post",
		url : config.update_status,
		data : { 
			entryId:gloablEntryId,
			errorCode:gloablErrorCode
		},
		error : function(request) {
			layer.alert("Connection error");
		},
		success : function(data){
			layer.alert(data.msg);
		}
	});
}

function dealRegular(t){
	if(t){
		var regularId = $(t).attr("regularId");
		$("#editTitle").html("修改解析");
		$("#commit").attr("type",regularId);
		
		$.ajax({
			cache : false,
			type : "GET",
			url : config.get_regular_list,
			data : { 
				regularId: $(t).attr("regularId")
			},
			error : function(request) {
				layer.alert("Connection error");
			},
			success : function(data){
				var item = data[0];
				console.info(item);
				$("#select").val(item.select);
				$("#attr").val(item.attr);
				$("#regex").val(item.regex);
				$("#regulartype").selectpicker('val', item.regulartype);
				$("#selectorType").selectpicker('val', item.selectorType);
				$("#desc").val(item.desc);
				$("#fieldtype").selectpicker('val', item.fieldtype);
				$("#dateformat").val(item.dateformat);
			}
		});
		
	}else{
		$("#editTitle").html("新增解析");
		$("#commit").attr("type","0");
		$("#select").val("");
		$("#attr").val("");
		$("#regex").val("");
		$("#regulartype").selectpicker('val', null);
		$("#selectorType").selectpicker('val', null);
		$("#desc").val("");
		$("#fieldtype").selectpicker('val', null);
		$("#dateformat").val("");
	}
	
}

$("#commit").click(function(){

	var regularId = $("#commit").attr("type");
	var select = $("#select").val();
	var attr = $("#attr").val();
	var regex = $("#regex").val();
	var regulartype = $("#regulartype").val();
	var selectorType = $("#selectorType").val();
	var desc = $("#desc").val();
	var fieldtype = $("#fieldtype").val();
	var dateformat = $("#dateformat").val();
	
	
	var data = {
		select: select,
		attr: attr,
		regex: regex,
		regulartype: regulartype,
		selectorType: selectorType,
		desc: desc,
		fieldtype: fieldtype,
		dateformat: dateformat,
		entryId: gloablEntryId,
		regularId: regularId
	};
	
	if(regularId==0){
		$.ajax({
			cache : true,
			type : "POST",
			url : config.save_regular,
			data : data, 
			async : false,
			error : function(data) {
				layer.alert(data.msg);
			},
			success : function(data) {
				layer.alert(data.msg);
			}
		});
		
	}else{
		$.ajax({
			cache : true,
			type : "POST",
			url : config.update_regular,
			data : data, 
			async : false,
			error : function(data) {
				layer.alert(data.msg);
			},
			success : function(data) {
				layer.alert(data.msg);
			}
		});
	}
});

$("#entryList").click(function(){
	var entryId = $("#entryList").val();
	regularData(entryId);	
});

function removeRegular(regularId,entryId){
	layer.confirm('确定要删除选中的记录？', {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			url : config.remove_regular,
			type : "post",
			data : { 
				regularId: regularId
			},
			success : function(r) {
				if (r.code == 0) {
					layer.msg(r.msg);
					regularData(entryId);
				} else {
					layer.msg(r.msg);
				}
			}
		});
	})
}